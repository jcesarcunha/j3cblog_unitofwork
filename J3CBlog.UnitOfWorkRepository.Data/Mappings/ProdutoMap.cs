﻿using FluentNHibernate.Mapping;
using J3CBlog.UnitOfWorkRepository.Core;

namespace J3CBlog.UnitOfWorkRepository.Data.Mappings
{
	public class ProdutoMap : ClassMap<Produto>
	{
		public ProdutoMap()
		{
			Id(p => p.Id);
			Map(p => p.Descricao);
			Map(p => p.Valor);
		}
	}
}