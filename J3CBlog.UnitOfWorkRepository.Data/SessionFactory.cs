﻿using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using J3CBlog.UnitOfWorkRepository.Data.Mappings;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace J3CBlog.UnitOfWorkRepository.Data
{
	public class SessionFactory
	{
		private static ISessionFactory _sessionFactory = null;

		public static ISessionFactory Create()
		{
			string connectionString = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
			if (_sessionFactory == null) {
				_sessionFactory = Fluently.Configure()
					.Database(
						MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
					.Mappings(m => m.FluentMappings.AddFromAssemblyOf<ProdutoMap>())
					.ExposeConfiguration(GerarEstruturaDoBancoDeDados)
					.BuildSessionFactory();
			}

			return _sessionFactory;
		}

		private static void GerarEstruturaDoBancoDeDados(NHibernate.Cfg.Configuration configuration)
		{
			new SchemaExport(configuration).Create(false, true);
		}
	}
}