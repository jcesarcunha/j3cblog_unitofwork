﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using J3CBlog.UnitOfWorkRepository.Core.Repository;
using NHibernate;
using NHibernate.Linq;

namespace J3CBlog.UnitOfWorkRepository.Data
{
	public sealed class Repository<T> : IRepository<T>
	{
		private ISession _session;

		public Repository(IUnitOfWork unitOfWork)
		{
			_session = unitOfWork.Sessao<ISession>();
		}

		public bool Incluir(T entidade)
		{
			_session.Save(entidade);
			return true;
		}

		public bool Incluir(IList<T> entidades)
		{
			foreach (var entidade in entidades) {
				Incluir(entidade);
			}
			return true;
		}

		public bool Atualizar(T entidade)
		{
			_session.Update(entidade);
			return true;
		}

		public bool Excluir(T entidade)
		{
			_session.Delete(entidade);
			return true;
		}

		public bool Excluir(IList<T> entidades)
		{
			foreach (var entidade in entidades) {
				Excluir(entidade);
			}
			return true;
		}

		public T SelecionarPorId(int id)
		{
			return _session.Get<T>(id);
		}

		public T SelecionarPor(Expression<Func<T, bool>> expressao)
		{
			return _session.Query<T>().Where(expressao).FirstOrDefault();
		}

		public IList<T> ListarPor(Expression<Func<T, bool>> expressao)
		{
			return _session.Query<T>().Where(expressao).ToList();
		}

		public IList<T> ListarTodos()
		{
			return _session.Query<T>().ToList();
		}
	}
}