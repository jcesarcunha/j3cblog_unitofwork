﻿using System;
using System.Data;
using J3CBlog.UnitOfWorkRepository.Core.Repository;
using NHibernate;

namespace J3CBlog.UnitOfWorkRepository.Data
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly ISessionFactory _sessionFactory;
		private readonly ITransaction _transaction;
		private ISession _session;

		public UnitOfWork()
		{
			_sessionFactory = SessionFactory.Create();
			_session = _sessionFactory.OpenSession();
			_session.FlushMode = FlushMode.Auto;
			_transaction = _session.BeginTransaction(IsolationLevel.ReadCommitted);
		}

		public TSession Sessao<TSession>()
		{
			if (typeof(TSession) == typeof(ISession)) {
				return (TSession)_session;
			}
			throw new InvalidCastException("TSession deve ser do tipo NHibernate.ISession");
		}

		public void Commit()
		{
			using (_transaction) {
				if (!_transaction.IsActive) {
					throw new InvalidOperationException("Erro! Nenhuma transação ativa.");
				}
				_transaction.Commit();
			}
		}

		public void Rollback()
		{
			if (_transaction.IsActive) {
				_transaction.Rollback();
			}
		}

		public void Dispose()
		{
			using (_session) {
				if (_session.IsOpen) {
					_session.Close();
				}
			}
		}
	}
}