﻿using System.Collections.Generic;
using J3CBlog.UnitOfWorkRepository.Core;
using J3CBlog.UnitOfWorkRepository.Core.Factories;
using J3CBlog.UnitOfWorkRepository.Core.Repository;
using NUnit.Framework;

namespace J3CBlog.UnitOfWorkRepository.Tests
{
	[TestFixture]
	public class RepositoryProdutoTests
	{
		private IUnitOfWork _unitOfWork;

		[SetUp]
		public virtual void SetUp()
		{
			_unitOfWork = Factory.CriarUnitOfWork();
		}

		[TearDown]
		public virtual void CommitUnitOfWork()
		{
			_unitOfWork.Commit();
			_unitOfWork.Dispose();
		}

		[Test]
		public void Incluir_Produto()
		{
			Produto produtoIncluido = null;
			Produto produto = new Produto { Descricao = "AC Brotherhood", Valor = 99.9m };
			IRepository<Produto> repositorioProduto = null;

			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);
				repositorioProduto.Incluir(produto);
				unitOfWork.Commit();
				produtoIncluido = repositorioProduto.SelecionarPorId(produto.Id);
			}

			Assert.That(produtoIncluido, Is.Not.Null);
			Assert.That(produtoIncluido.Descricao, Is.EqualTo(produto.Descricao));
		}

		[Test]
		public void Atualizar_Produto()
		{
			Produto produtoParaAtualizar = null;
			Produto produto = new Produto { Descricao = "AC Revelations", Valor = 119.9m };
			IRepository<Produto> repositorioProduto = null;

			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);
				repositorioProduto.Incluir(produto);

				produtoParaAtualizar = repositorioProduto.SelecionarPorId(produto.Id);
				produtoParaAtualizar.Descricao = "AC Revelations - Signature Edition";
				produtoParaAtualizar.Valor = 130m;

				repositorioProduto.Atualizar(produtoParaAtualizar);

				unitOfWork.Commit();
			}

			Assert.That(produtoParaAtualizar.Descricao, Is.EqualTo("AC Revelations - Signature Edition"));
			Assert.That(produtoParaAtualizar.Valor, Is.EqualTo(130m));
		}

		[Test]
		public void Excluir_Produto()
		{
			Produto produtoParaExcluir = null;
			Produto produto = new Produto { Descricao = "AC III", Valor = 179.9m };
			IRepository<Produto> repositorioProduto = null;

			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);
				repositorioProduto.Incluir(produto);

				produtoParaExcluir = repositorioProduto.SelecionarPorId(produto.Id);

				Assert.That(produtoParaExcluir, Is.Not.Null);

				repositorioProduto.Excluir(produtoParaExcluir);
				unitOfWork.Commit();
			}

			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);
				Produto produtoExcluido = repositorioProduto.SelecionarPorId(produto.Id);
				
				Assert.That(produtoExcluido, Is.Null);
			}
		}

		[Test]
		public void Listar_Produtos()
		{
			IRepository<Produto> repositorioProduto = null;
			List<Produto> todosOsProdutos = new List<Produto>();
			List<Produto> produtosCadastrados = new List<Produto>();
			List<Produto> novosProdutos = new List<Produto>
			{
				new Produto { Descricao = "AC Brotherhood", Valor = 99.9m },
				new Produto { Descricao = "AC Revelations", Valor = 119.9m },
				new Produto { Descricao = "AC III", Valor = 179.9m }
			};

			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);

				produtosCadastrados = repositorioProduto.ListarTodos() as List<Produto>;
				
				repositorioProduto.Incluir(novosProdutos);

				todosOsProdutos = repositorioProduto.ListarTodos() as List<Produto>;

				unitOfWork.Commit();
			}

			Assert.That(produtosCadastrados.Count + novosProdutos.Count, Is.EqualTo(todosOsProdutos.Count));
		}
	}
}