﻿using System.Collections.Generic;
using J3CBlog.UnitOfWorkRepository.Core;
using NUnit.Framework;

namespace J3CBlog.UnitOfWorkRepository.Tests
{
	[TestFixture]
	public class ServicoProdutoTests
	{
		[Test]
		public void Listar_Produtos_Em_Pedidos()
		{
			ServicoProduto servicoProduto = new ServicoProduto();
			List<Produto> produtos = servicoProduto.ListarProdutosEmPedidos();

			Assert.That(produtos.Count, Is.EqualTo(5));
		}
	}
}