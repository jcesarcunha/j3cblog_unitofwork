﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace J3CBlog.UnitOfWorkRepository.Core.Repository
{
	public interface IRepository<T>
	{
		bool Incluir(T entidade);
		bool Incluir(IList<T> entidades);
		bool Atualizar(T entidade);
		bool Excluir(T entidade);
		bool Excluir(IList<T> entidades);

		T SelecionarPorId(int id);
		T SelecionarPor(Expression<Func<T, bool>> expressao);
		IList<T> ListarPor(Expression<Func<T, bool>> expressao);
		IList<T> ListarTodos();
	}
}