﻿using System;

namespace J3CBlog.UnitOfWorkRepository.Core.Repository
{
	public interface IUnitOfWork : IDisposable
	{
		TSessao Sessao<TSessao>();
		void Commit();
		void Rollback();
	}
}