﻿using System.Collections.Generic;
using J3CBlog.UnitOfWorkRepository.Core.Factories;
using J3CBlog.UnitOfWorkRepository.Core.Repository;

namespace J3CBlog.UnitOfWorkRepository.Core
{
	public class ServicoProduto
	{
		public List<Produto> ListarProdutosEmPedidos()
		{
			using (IUnitOfWork unitOfWork = Factory.CriarUnitOfWork()) {
				IRepository<Produto> repositorioProduto = Factory.CriarRepositorioPara<Produto>(unitOfWork);
				return repositorioProduto.ListarTodos() as List<Produto>;
			}
		}
	}
}