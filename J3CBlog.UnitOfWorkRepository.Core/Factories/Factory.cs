﻿using System.Configuration;
using J3CBlog.UnitOfWorkRepository.Core.Repository;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace J3CBlog.UnitOfWorkRepository.Core.Factories
{
	public class Factory
	{
		private static IUnityContainer _container;
		private static UnityConfigurationSection _section;

		public static IUnitOfWork CriarUnitOfWork()
		{
			Init();
			return _container.Resolve<IUnitOfWork>();
		}

		public static IRepository<T> CriarRepositorioPara<T>(IUnitOfWork unitOfWork)
		{
			Init();
			return _container.Resolve<IRepository<T>>(new ResolverOverride[] { new ParameterOverride("unitOfWork", unitOfWork) });
		}

		private static void Init()
		{
			_container = new UnityContainer();
			_section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
			_section.Configure(_container);
		}
	}
}