﻿namespace J3CBlog.UnitOfWorkRepository.Core
{
	public class Produto
	{
		public virtual int Id { get; set; }
		public virtual string Descricao { get; set; }
		public virtual decimal Valor { get; set; }
	}
}